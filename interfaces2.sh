 #! /bin/sh
cat > /etc/sysconfig/network-scripts/ifcfg-eth1 <<EOF
DEVICE="eth1"
BOOTPROTO="dhcp"
IPV6INIT="yes"
MTU="1500"
NM_CONTROLLED="yes"
ONBOOT="yes"
TYPE="Ethernet"
EOF
cat > /etc/sysconfig/network-scripts/ifcfg-eth2 <<EOF
DEVICE="eth2"
BOOTPROTO="dhcp"
IPV6INIT="yes"
MTU="1500"
NM_CONTROLLED="yes"
ONBOOT="yes"
TYPE="Ethernet"
EOF
ifup eth1
ifup eth2
ip route delete default
ip route add default via 192.168.120.1 dev eth0